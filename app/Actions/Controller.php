<?php namespace App\Actions;


use App\Classes\Book;
use App\Classes\CD;
use App\Classes\DB;
use App\Classes\Furniture;

class Controller
{

    private $request_method;
    private $request_action;
    private $payload;
//Construct function for request method/action;
    public function __construct()
    {
        $this->request_method = strtolower($_SERVER['REQUEST_METHOD']);
        $this->request_action = substr(strtolower($_SERVER['REQUEST_URI']), 1);
        if($this->request_action === '' || $this->request_action === '/'){
            header('Location: /show');
            return;
        }


        $this->payload = $_REQUEST;

    }

//4 request action delete/add/show/notFound;

//UPDATED PART OF THE CODE BELOW
    public function index(){
        $method =  sprintf('%sAction', $this->request_action);
        if(method_exists($this,$method)){
            call_user_func([
                self::class,
                $method
            ]);
        }else{
            $this->showNotFound();
        }

    }
    private function showAction(){
        $db = new DB();
        $data = $db->query('SELECT * FROM list');
        $data = array_map(function($item){
            $class = 'App\Classes\\'.$item['product_type'];
            try {
                return new $class($item);
            }catch (\Exception $e){
                return null;
            }
        }, $data);

        $data = array_filter($data);

        $this->view('show', $data);
    }

//deleteAction, checked items delete by id;
    private function deleteAction(){
        if($this->request_method === 'post'){

            if(isset($_POST['deleteMultipleBtn'])){
                $ids = $this->payload['records'];
                if(is_array($ids) && !empty($ids)){
                    $ids = array_map(function($item){
                        return is_string($item) ? $item : $item . '';
                    }, $ids);
                    $db = new DB();
                    $db->query("DELETE FROM list WHERE id in (".implode(',', $ids).")");
                }

                header('Location: /show');

            }
        }
    }
//addAction function;
    private function addAction(){
        if($this->request_method === 'get'){
            $this->view('add_product_form');
            return;
        }


        if($this->request_method === 'post'){

            $product_to_insert = [
              'product_sku' => $this->payload['product_sku'] ?? null,
              'product_name' => $this->payload['product_name'] ?? null,
              'product_price' => $this->payload['product_price'] ?? null,
              'product_type' => $this->payload['product_type'] ?? null,
              'product_size' =>   $this->payload['product_size']
            ];

            if(strtolower($this->payload['product_type']) === 'furniture'){
                $product_to_insert['product_size'] = sprintf('%s %s %s',
                    $this->payload['product_dem1'], $this->payload['product_dem2'], $this->payload['product_dem3']);
                $item = new Furniture($product_to_insert);
            }elseif(strtolower($this->payload['product_type']) === 'book'){
                $product_to_insert['product_size'] = $this->payload['product_kg'];
                $item = new Book($product_to_insert);
            }else{
                $item = new CD($product_to_insert);
            }


            $item->save();
            header('Location: /show');
            return;
        }
    }
//showNotFound, wrong url;
    private function showNotFound(){
        $this->view('not_found');
        return;
    }


    private function view(string $viewFileName, $data = null){
        require_once __DIR__.'/../Views/'.$viewFileName.'.php';
    }

}
