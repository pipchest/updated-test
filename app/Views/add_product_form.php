<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add Product</title>
<!------------------------------------------------------------Links------------------------------------------------------------>
<link rel="stylesheet" href="styles.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</head>
<body>
<!-----------------------------------------------------------Header------------------------------------------------------------>
<header class="bg-light header">
    <h1 class="add">Product Add</h1>
    <a class="addproduct" href="/show" target="_self"><i class="fas fa-plus"></i>Product List</a>
</header>
<!------------------------------------------------------------Main------------------------------------------------------------>
<main class="main">
    <div class="d-flex justify-content-center">
    <form action="/add" method="post"  class="w-50 text-center">
<!--------------------------------------------------------Basic Inputs------------------------------------------------------------>

            <div class="form-group">
                <label for="formGroupExampleInput">SKU</label>
                <input type="text" class="form-control" name="product_sku" placeholder="Enter the SKU">
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput2">Name</label>
                <input type="text" class="form-control" name="product_name" placeholder="Enter the Name" >
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput2">Price</label>
                <input type="text" class="form-control" name="product_price" placeholder="Enter the Price">
            </div>
            <div class="form-group">
                <input id="product-type" type="hidden" class="form-control" name="product_type" placeholder="Enter the Price">
            </div>
    </div>
    <div>
        <div class="btn-group type-switcher d-flex flex-column w-50 text-center justify-content-center">
<!------------------------------------------------------------Type Switcher------------------------------------------------------------>
            <label class="typeSwitcher-label" for="">Choose type of product</label>
            <button type="button" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Type Switcher
            </button>
<!----------------------------------------------------------------Menu-------------------------------------------------------------->
            <div class="dropdown-menu">
                <a class="dropdown-item" onclick="openForm1()">CD</a>
                <a class="dropdown-item" onclick="openForm2()" >Furniture</a>
                <a class="dropdown-item" onclick="openForm3()" >Book</a>
            </div>
</div>
<!----------------------------------------------------------------Forms--------------------------------------------------------------->
        <div  class="formone text-center justify-content-center w-50">
            <div class="formone text-center justify-content-center w-50">
                <div id="firstform" class="size">
                    <div class="form-group-one">
                        <label for="formGroupExampleInput">Size</label>
                        <input type="text" class="form-control" name="product_size" placeholder="Enter the Size">
                        <p class="dimensions">Please provide dimensions in MB format</p>
                    </div>
                </div>
            </div>
            <div class="formone text-center justify-content-center w-50">
                <div id="secondform" class="hwl">
                    <div class="form-group-one">
                        <label for="formGroupExampleInput">Height</label>
                        <input type="text" name="product_dem1" class="form-control" placeholder="Enter the Height">
                        <label for="formGroupExampleInput">Width</label>
                        <input type="text" name="product_dem2" class="form-control" placeholder="Enter the Width">
                        <label for="formGroupExampleInput">Length</label>
                        <input type="text" name="product_dem3" class="form-control" placeholder="Enter the Length">
                        <p class="dimensions">Please provide dimensions in HxWxL format</p>
                    </div>
                </div>
            </div>
            <div class="formone text-center justify-content-center w-50">
                <div id="thirdform" class="size>
                    <div class="form-group-one">
                <label for="formGroupExampleInput">Weight</label>
                <input type="text" name="product_kg" class="form-control" placeholder="Enter the Weight">
                <p class="dimensions">Please provide dimensions in KG format</p>
            </div>
        </div>
    </div>
    </div>
    <button type="submit" name="submit"  class="hello btn btn-outline-secondary">Submit</button>
    </div>
    </form>
</main>


<!-------------------------------------------------------Main-End------------------------------------------------------------->
<script src="https://kit.fontawesome.com/e42b41514a.js" crossorigin="anonymous"></script>
<script src="index.js"></script>
</body>
</html>
