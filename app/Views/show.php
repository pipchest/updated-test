<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ProductList</title>
    <link rel="stylesheet" href="styles.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
<body>
<div class=""></div>
<header class="bg-light header">
    <h1 class="h-text">Product List</h1>
    <a class="addproduct" href="/add" target="_self"><i class="fas fa-plus"></i>Add Product</a>

</header>

<main>
    <div class="container">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr class="bg-dark text-center">
                    <th class="text-light" scope="col">SKU</th>
                    <th class="text-light">Name</th>
                    <th class="text-light">Price</th>
                    <th class="text-light">Size</th>
                    <th class="text-light">Product Type</th>
                    <th class="text-light">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php

                foreach ($data as $item):
                    if(empty($item)){
                        continue;
                    }
                    ?>
                    <tr class="text-center">
                        <td><?php echo $item->sku; ?></td>
                        <td><?php echo $item->name; ?></td>
                        <td><?php echo $item->price; ?></td>
                        <td><?php echo $item->size; ?></td>
                        <td><?php echo $item->type; ?></td>
                        <td><form action="/delete" method="post">
                                <input type="checkbox" name="records[]" value="<?php echo $item->id; ?>">
                        </td>

                    </tr>

                <?php
                endforeach;
                ?>

                </tbody>
            </table>
            <div class="text-center">
            <input type="submit" name="deleteMultipleBtn" value="DELETE" class="btn btn-outline-danger">
            </div>
            </form>
        </div>
    </div>

</main>

<script src="https://kit.fontawesome.com/e42b41514a.js" crossorigin="anonymous"></script>
<script src="../productlist/index.js"></script>
</body>
</html>