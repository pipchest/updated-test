<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ProductList</title>
    <link rel="stylesheet" href="styles.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
<body>
<div class=""></div>
<header class="bg-light header">
    <h1 class="h-text">Product List</h1>
    <a class="addproduct" href="/add" target="_self"><i class="fas fa-plus"></i>Add Product</a>

</header>

<main>
    <div class="container">
        <div class="table-responsive">
            <h1>Requested page was not found.</h1>
            <a href="/show">Redirecting to main page...</a>
        </div>
    </div>

</main>
<!--not_found.php script -->
<script type="text/javascript">
    window.setTimeout(() => {
            window.location.href = '/show';
        },
    3000)
</script>
</body>
</html>