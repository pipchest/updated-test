<?php
namespace App\Classes;

//Abstract class Product which es extended by Book,CD and Furniture classes;
abstract class Product {
    public $id;
    public $sku;
    public $price;
    public $size;
    public $size_raw;
    public $name;
    public $type;


    public function __construct(array $product)
    {
        $this->id = $product['id'];
        $this->sku = $product['product_sku'];
        $this->price = $product['product_price'];
        $this->name = $product['product_name'];
        $this->size_raw = $product['product_size'];
        $this->type = $product['product_type'];

    }

    abstract public function setSizeAttribute();
//Save function for insert data in db;
    public function save(){
        $db = new DB();
        $db->query("INSERT INTO list (product_sku, product_name, product_price, product_type, product_size) 
          VALUES ('$this->sku', '$this->name', '$this->price', '$this->type', '$this->size_raw');");
    }

}

