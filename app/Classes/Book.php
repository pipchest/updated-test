<?php namespace App\Classes;

//Book class which extends abstract class Product;
class Book extends Product{

    public function __construct(array $product)
    {
        parent::__construct($product);
        $this->setSizeAttribute();
    }
//Getting KG size attribute by setSizeAttribute;
    public function setSizeAttribute(){
        $this->size = "{$this->size_raw}KG";
    }
}
