<?php namespace App\Classes;
use PDO;
class DB
{
    # @var, MySQL Hostname
    private $hostname = 'localhost';
    # @var, MySQL Database
    private $database = 'product_list';
    # @var, MySQL Username
    private $username = 'root';
    # @var, MySQL Password
    private $password = '';
    # @object, The PDO object
    private $pdo;
    # @object, PDO statement object
    private $sQuery;
    # @bool ,  Connected to the database
    private $bConnected = false;
    # @array, The parameters of the SQL query
    private $parameters;

    public function __construct()
    {
        $this->Connect($this->hostname, $this->database, $this->username, $this->password);
        $this->parameters = array();
    }

    private function Connect($hostname, $database, $username, $password)
    {

        $dsn = 'mysql:dbname=' . $database . ';host=' . $hostname;
        try {

            $this->pdo = new PDO($dsn, $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));


            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


            $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);

            $this->bConnected = true;
        } catch (PDOException $e) {

            echo $this->ExceptionLog($e->getMessage());
            die();
        }
    }

    public function CloseConnection()
    {

        $this->pdo = null;
    }

    private function Init($query, $parameters = "")
    {
        # Connect to database
        if (!$this->bConnected) {
            $this->Connect();
        }
        try {
            // Prepare query
            $this->sQuery = $this->pdo->prepare($query);

            // Add parameters to the parameter array
            $this->bindMore($parameters);
            // Bind parameters
            if (!empty($this->parameters)) {
                foreach ($this->parameters as $param) {
                    $parameters = explode("\x7F", $param);
                    $this->sQuery->bindParam($parameters[0], $parameters[1]);
                }
            }
            // Execute SQL
            $this->success = $this->sQuery->execute();
        } catch (PDOException $e) {
            // Write into log and display Exception
            $this->ExceptionLog($e->getMessage(), $query);
        }
        // Reset the parameters
        $this->parameters = array();
    }

    public function bind($para, $value)
    {
        $this->parameters[sizeof($this->parameters)] = ":" . $para . "\x7F" . utf8_encode($value);
    }

    public function bindMore($parray)
    {
        if (empty($this->parameters) && is_array($parray)) {
            $columns = array_keys($parray);
            foreach ($columns as $i => &$column) {
                $this->bind($column, $parray[$column]);
            }
        }
    }

    public function query($query, $params = null, $fetchmode = PDO::FETCH_ASSOC)
    {
        $query = trim($query);
        $this->Init($query, $params);
        $rawStatement = explode(" ", $query);

        // Which SQL statement is used
        $statement = strtolower($rawStatement[0]);

        if ($statement === 'select' || $statement === 'show') {
            return $this->sQuery->fetchAll($fetchmode);
        } elseif ($statement === 'insert' || $statement === 'update' || $statement === 'delete') {
            return $this->sQuery->rowCount();
        } else {
            return NULL;
        }
    }


}