<?php namespace App\Classes;

//CD class which extends abstract class Product;
class CD extends Product{
    public function __construct(array $product)
    {
        parent::__construct($product);
        $this->setSizeAttribute();
    }
//returns MB size attribute by setSizeAttribute;
    public function setSizeAttribute(){
        $this->size = "{$this->size_raw}MB";
    }
}
