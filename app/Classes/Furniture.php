<?php namespace App\Classes;

//Book class which extends abstract class Product;
class Furniture extends Product{

    public function __construct(array $product)
    {
        parent::__construct($product);
        $this->setSizeAttribute();
    }
//Returns HxWxL format by setSizeAttribute;
    public function setSizeAttribute(){
        //{height}x{width}x{length}
        $this->size = implode('x',explode(' ', $this->size_raw));
    }

}
