<?php namespace App;

use App\Actions\Controller;

class App
{
   public $basePath;
   public $controller;

   public function __construct()
   {
        $this->basePath = dirname(__DIR__);
        $this->controller = new Controller();
   }


   public function run(){
        $this->controller->index();
   }
}
